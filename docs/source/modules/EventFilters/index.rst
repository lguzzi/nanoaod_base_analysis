Event Filters
=============

Installation
------------

``git clone https://gitlab.cern.ch/cms-phys-ciemat/event-filters.git Event/Filters``

RDFModules
----------

.. automodule:: Event.Filters

.. toctree::
    :maxdepth: 2
   
    METfilters
    lumiFilter
