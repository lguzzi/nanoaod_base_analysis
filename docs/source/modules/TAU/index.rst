TAU
===

Installation
------------

``git clone https://gitlab.cern.ch/cms-phys-ciemat/tau-corrections.git Corrections/TAU``

RDFModules
----------

.. toctree::
    :maxdepth: 3

    tau
