HH->bbtt
========

Installation
------------

``git clone https://github.com/jaimeleonh/InferenceTools.git Tools/Tools``

RDFModules
----------

.. toctree::
    :maxdepth: 3

    hhlepton
    hhjets
    hhdnn
    hhmulticlass
    idiso 
